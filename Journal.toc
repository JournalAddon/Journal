## Interface: 80000
## Title: |c000088ffJournal|r
## Notes: A journal with shareable entries
## Author: Simon Hyll
## Version: @project-version@
## X-Curse-Project-ID: 297779
## DefaultState: enabled
## SavedVariablesPerCharacter: Journal_Settings, Journal_Entries

Main.xml
