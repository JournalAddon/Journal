-- Shows the Journal window
SLASH_JOURNAL1 = "/journal";
SLASH_JOURNAL2 = "/jou";
SLASH_JOURNAL3 = "/j";
function SlashCmdList.JOURNAL(msg)
    JournalEvents.fireEvent("TOGGLE_JOURNAL");
end

-- Shows the Journal window
SLASH_JOURNALVERSION1 = "/journalv";
SLASH_JOURNALVERSION2 = "/jouv";
SLASH_JOURNALVERSION3 = "/jv";
function SlashCmdList.JOURNALVERSION(msg)
    print("|c000088ffJournal|r version: " .. GetAddOnMetadata("Journal", "Version"));
end
