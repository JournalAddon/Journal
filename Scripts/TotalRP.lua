-- Adds Journal to TotalRP
local EventFrame = CreateFrame("FRAME"); -- Need a frame to respond to events
EventFrame:RegisterEvent("ADDON_LOADED"); -- Fired when saved variables are loaded

EventFrame:SetScript('OnEvent', function(self, event, ...)
    self[event](self, ...)
end)

function EventFrame:ADDON_LOADED(arg1)
    if (arg1 == "Total RP 3") then
        message("TOTAL RP 3 DETECTED!!!");
    end
end
