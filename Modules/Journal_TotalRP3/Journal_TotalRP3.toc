## Interface: 80000
## Title: |c000088ffJournal|r: TotalRP3
## Notes: Extends TotalRP3 with an integration for Journal
## Author: Simon Hyll
## Version: @project-version@
## X-Curse-Project-ID: 297779
## RequiredDeps: Journal, totalRP3
## DefaultState: enabled
## SavedVariablesPerCharacter: Journal_Settings, Journal_Entries

Extension.xml
