function SetJournalVariables()
    ----------------------------------------------------------------
    -- Settings
    ----------------------------------------------------------------
    local Journal_SettingsDefaults = {
        MinimapPos = 90
    }
    Journal_Settings = Journal_Settings or Journal_SettingsDefaults;

    ----------------------------------------------------------------
    -- Entries
    ----------------------------------------------------------------
    local Journal_EntriesDefaults = {
        page_1 = "Start typing..."
    }
    Journal_Entries = Journal_Entries or Journal_EntriesDefaults;
end
