-- Player enters the world
JournalEvents.registerEvent("PLAYER_LOGIN", "welcome", function (...)
    print("|c000088ffJournal|r version: " .. GetAddOnMetadata("Journal", "Version"));
end);

-- All the events registered by Journal
JournalEvents.registerEvent("SHOW_JOURNAL", "show", function ()
    -- Show Journal
    if not JournalFrame:IsShown() == true then
        JournalFrame:Show();
    end
end);
JournalEvents.registerEvent("HIDE_JOURNAL", "hide", function ()
    -- Hide Journal
    if JournalFrame:IsShown() == true then
        JournalFrame:Hide()
    end
end);
JournalEvents.registerEvent("TOGGLE_JOURNAL", "toggle", function ()
    -- Toggle Journal
    if JournalFrame:IsShown() == true then
        JournalFrame:Hide()
    else
        JournalFrame:Show();
    end
end);

JTestVar = {};

-- Addon is loaded
JournalEvents.registerEvent("ADDON_LOADED", "load", function(self, arg1)
    if (arg1 == L["Journal"]) then
        SetJournalVariables();

        Journal_MinimapButton_Reposition();

        JournalInterfaceOptions.name = "Journal";
        InterfaceOptions_AddCategory(JournalInterfaceOptions);
    end

    if (arg1 == "totalRP3_Extended") then
        loaded, reason = LoadAddOn("Journal_TotalRP3")
    end
end)
