## Interface: 80000
## Title: |c000088ffJournal|r: Standalone
## Notes: The standalone UI for Journal
## Author: Simon Hyll
## Version: @project-version@
## X-Curse-Project-ID: 297779
## RequiredDeps: Journal
## DefaultState: enabled
## SavedVariablesPerCharacter: Journal_Settings, Journal_Entries

Main.xml
