-- Simple little library for event handling
JournalEvents = {};

JournalEvents.events = {};

-- Fire an event
JournalEvents.fireEvent = function(name, ...)
    for index, entry in pairs(JournalEvents.events[name]) do
        entry.func(...);
    end
end

-- Run a specific registered funciton
JournalEvents.runFunc = function(event, name, ...)
    JournalEvents.events[event][name].func(...);
end

JournalEvents.registerEvent = function(event, name, func)
    -- Add function to event group
    if JournalEvents.events[event] == nil then
        JournalEvents.events[event] = {};
    end
    JournalEvents.events[event][name] = {};
    JournalEvents.events[event][name]["func"] = func;
end

-- Unregisters an existing event
JournalEvents.unregister = function(event, name)
    if not (JournalEvents.events[event][name] == nil) or (JournalEvents.events[event][name] == '') then
        JournalEvents.events[event][name] = nil;
    end
end

JournalEvents.unregisterAll = function(event)
    if not (JournalEvents.events[event] == nil) or (JournalEvents.events[event] == '') then
        JournalEvents.events[event] = nil;
    end
end

local EventFrame = CreateFrame("FRAME"); -- Need a frame to respond to client events

-- All the WoW events a frame can listen for
-- http://wowwiki.wikia.com/wiki/Events_A-Z_(Full_List)

EventFrame:RegisterEvent("ACHIEVEMENT_EARNED");
EventFrame:RegisterEvent("ACTIONBAR_HIDEGRID");
EventFrame:RegisterEvent("ACTIONBAR_PAGE_CHANGED");
EventFrame:RegisterEvent("ACTIONBAR_SHOWGRID");
EventFrame:RegisterEvent("ACTIONBAR_SLOT_CHANGED");
EventFrame:RegisterEvent("ACTIONBAR_UPDATE_COOLDOWN");
EventFrame:RegisterEvent("ACTIONBAR_UPDATE_STATE");
EventFrame:RegisterEvent("ACTIONBAR_UPDATE_USABLE");
EventFrame:RegisterEvent("ACTIVE_TALENT_GROUP_CHANGED");
EventFrame:RegisterEvent("ADDON_ACTION_BLOCKED");
EventFrame:RegisterEvent("ADDON_ACTION_FORBIDDEN");
EventFrame:RegisterEvent("ADDON_LOADED"); -- Fired whenever an addon is loaded, arg1 = name of addon
EventFrame:RegisterEvent("AREA_SPIRIT_HEALER_IN_RANGE");
EventFrame:RegisterEvent("AREA_SPIRIT_HEALER_OUT_OF_RANGE");
EventFrame:RegisterEvent("ARENA_OPPONENT_UPDATE");
EventFrame:RegisterEvent("ARENA_SEASON_WORLD_STATE");
EventFrame:RegisterEvent("AUCTION_BIDDER_LIST_UPDATE");
EventFrame:RegisterEvent("AUCTION_HOUSE_CLOSED");
EventFrame:RegisterEvent("AUCTION_HOUSE_DISABLED");
EventFrame:RegisterEvent("AUCTION_HOUSE_SHOW");
EventFrame:RegisterEvent("AUCTION_ITEM_LIST_UPDATE");
EventFrame:RegisterEvent("AUCTION_MULTISELL_FAILURE");
EventFrame:RegisterEvent("AUCTION_MULTISELL_START");
EventFrame:RegisterEvent("AUCTION_MULTISELL_UPDATE");
EventFrame:RegisterEvent("AUCTION_OWNED_LIST_UPDATE");
EventFrame:RegisterEvent("AUTOFOLLOW_BEGIN");
EventFrame:RegisterEvent("AUTOFOLLOW_END");

EventFrame:RegisterEvent("BAG_CLOSED");
EventFrame:RegisterEvent("BAG_OPEN");
EventFrame:RegisterEvent("BAG_UPDATE");
EventFrame:RegisterEvent("BAG_UPDATE_COOLDOWN");
EventFrame:RegisterEvent("BANKFRAME_CLOSED");
EventFrame:RegisterEvent("BANKFRAME_OPENED");
EventFrame:RegisterEvent("BARBER_SHOP_APPEARANCE_APPLIED");
EventFrame:RegisterEvent("BARBER_SHOP_CLOSE");
EventFrame:RegisterEvent("BARBER_SHOP_OPEN");
EventFrame:RegisterEvent("BARBER_SHOP_SUCCESS");
EventFrame:RegisterEvent("BATTLEFIELDS_CLOSED");
EventFrame:RegisterEvent("BATTLEFIELDS_SHOW");
EventFrame:RegisterEvent("BILLING_NAG_DIALOG");
EventFrame:RegisterEvent("BIND_ENCHANT");
EventFrame:RegisterEvent("BN_BLOCK_LIST_UPDATED");
EventFrame:RegisterEvent("BN_CONNECTED");
EventFrame:RegisterEvent("BN_CUSTOM_MESSAGE_CHANGED");
EventFrame:RegisterEvent("BN_CUSTOM_MESSAGE_LOADED");
EventFrame:RegisterEvent("BN_DISCONNECTED");
EventFrame:RegisterEvent("BN_FRIEND_ACCOUNT_OFFLINE");
EventFrame:RegisterEvent("BN_FRIEND_ACCOUNT_ONLINE");
EventFrame:RegisterEvent("BN_FRIEND_INFO_CHANGED");
EventFrame:RegisterEvent("BN_FRIEND_INVITE_ADDED");
EventFrame:RegisterEvent("BN_FRIEND_INVITE_LIST_INITIALIZED");
EventFrame:RegisterEvent("BN_FRIEND_INVITE_REMOVED");
EventFrame:RegisterEvent("BN_FRIEND_LIST_SIZE_CHANGED");
EventFrame:RegisterEvent("BN_REQUEST_FOF_SUCCEEDED");

EventFrame:RegisterEvent("CALENDAR_ACTION_PENDING");
EventFrame:RegisterEvent("CALENDAR_CLOSE_EVENT");
EventFrame:RegisterEvent("CALENDAR_EVENT_ALARM");
EventFrame:RegisterEvent("CALENDAR_NEW_EVENT");
EventFrame:RegisterEvent("CALENDAR_OPEN_EVENT");
EventFrame:RegisterEvent("CALENDAR_UPDATE_INVITE_LIST");
EventFrame:RegisterEvent("CALENDAR_UPDATE_ERROR");
EventFrame:RegisterEvent("CALENDAR_UPDATE_EVENT");
EventFrame:RegisterEvent("CALENDAR_UPDATE_EVENT_LIST");
EventFrame:RegisterEvent("CALENDAR_UPDATE_PENDING_INVITES");
EventFrame:RegisterEvent("CANCEL_LOOT_ROLL");
EventFrame:RegisterEvent("CANCEL_SUMMON");
EventFrame:RegisterEvent("CHANNEL_COUNT_UPDATE");
EventFrame:RegisterEvent("CHANNEL_FLAGS_UPDATED");
EventFrame:RegisterEvent("CHANNEL_INVITE_REQUEST");
EventFrame:RegisterEvent("CHANNEL_PASSWORD_REQUEST");
EventFrame:RegisterEvent("CHANNEL_ROSTER_UPDATE");
EventFrame:RegisterEvent("CHANNEL_UI_UPDATE");
EventFrame:RegisterEvent("CHARACTER_POINTS_CHANGED");
EventFrame:RegisterEvent("CHAT_MSG_ACHIEVEMENT");
EventFrame:RegisterEvent("CHAT_MSG_ADDON");
EventFrame:RegisterEvent("CHAT_MSG_AFK");
EventFrame:RegisterEvent("CHAT_MSG_BG_SYSTEM_ALLIANCE");
EventFrame:RegisterEvent("CHAT_MSG_BG_SYSTEM_HORDE");
EventFrame:RegisterEvent("CHAT_MSG_BG_SYSTEM_NEUTRAL");
EventFrame:RegisterEvent("CHAT_MSG_BN_INLINE_TOAST_ALERT");
EventFrame:RegisterEvent("CHAT_MSG_BN_INLINE_TOAST_BROADCAST");
EventFrame:RegisterEvent("CHAT_MSG_BN_INLINE_TOAST_BROADCAST_INFORM");
EventFrame:RegisterEvent("CHAT_MSG_BN_INLINE_TOAST_CONVERSATION");
EventFrame:RegisterEvent("CHAT_MSG_BN_WHISPER");
EventFrame:RegisterEvent("CHAT_MSG_BN_WHISPER_INFORM");
EventFrame:RegisterEvent("CHAT_MSG_CHANNEL");
EventFrame:RegisterEvent("CHAT_MSG_CHANNEL_JOIN");
EventFrame:RegisterEvent("CHAT_MSG_CHANNEL_LEAVE");
EventFrame:RegisterEvent("CHAT_MSG_CHANNEL_LIST");
EventFrame:RegisterEvent("CHAT_MSG_CHANNEL_NOTICE");
EventFrame:RegisterEvent("CHAT_MSG_CHANNEL_NOTICE_USER");
EventFrame:RegisterEvent("CHAT_MSG_COMBAT_FACTION_CHANGE");
EventFrame:RegisterEvent("CHAT_MSG_COMBAT_HONOR_GAIN");
EventFrame:RegisterEvent("CHAT_MSG_COMBAT_MISC_INFO");
EventFrame:RegisterEvent("CHAT_MSG_COMBAT_XP_GAIN");
EventFrame:RegisterEvent("CHAT_MSG_DND");
EventFrame:RegisterEvent("CHAT_MSG_EMOTE");
EventFrame:RegisterEvent("CHAT_MSG_FILTERED");
EventFrame:RegisterEvent("CHAT_MSG_GUILD");
EventFrame:RegisterEvent("CHAT_MSG_GUILD_ACHIEVEMENT");
EventFrame:RegisterEvent("CHAT_MSG_IGNORED");
EventFrame:RegisterEvent("CHAT_MSG_LOOT");
EventFrame:RegisterEvent("CHAT_MSG_MONEY");
EventFrame:RegisterEvent("CHAT_MSG_MONSTER_EMOTE");
EventFrame:RegisterEvent("CHAT_MSG_MONSTER_SAY");
EventFrame:RegisterEvent("CHAT_MSG_MONSTER_WHISPER");
EventFrame:RegisterEvent("CHAT_MSG_MONSTER_PARTY");
EventFrame:RegisterEvent("CHAT_MSG_MONSTER_YELL");
EventFrame:RegisterEvent("CHAT_MSG_OFFICER");
EventFrame:RegisterEvent("CHAT_MSG_OPENING");
EventFrame:RegisterEvent("CHAT_MSG_PARTY");
EventFrame:RegisterEvent("CHAT_MSG_PARTY_LEADER");
EventFrame:RegisterEvent("CHAT_MSG_PET_INFO");
EventFrame:RegisterEvent("CHAT_MSG_RAID");
EventFrame:RegisterEvent("CHAT_MSG_RAID_BOSS_EMOTE");
EventFrame:RegisterEvent("CHAT_MSG_RAID_BOSS_WHISPER");
EventFrame:RegisterEvent("CHAT_MSG_RAID_LEADER");
EventFrame:RegisterEvent("CHAT_MSG_RAID_WARNING");
EventFrame:RegisterEvent("CHAT_MSG_RESTRICTED");
EventFrame:RegisterEvent("CHAT_MSG_SAY");
EventFrame:RegisterEvent("CHAT_MSG_SKILL");
EventFrame:RegisterEvent("CHAT_MSG_SYSTEM");
EventFrame:RegisterEvent("CHAT_MSG_TARGETICONS");
EventFrame:RegisterEvent("CHAT_MSG_TRADESKILLS");
EventFrame:RegisterEvent("CHAT_MSG_TEXT_EMOTE");
EventFrame:RegisterEvent("CHAT_MSG_WHISPER");
EventFrame:RegisterEvent("CHAT_MSG_WHISPER_INFORM");
EventFrame:RegisterEvent("CHAT_MSG_YELL");
EventFrame:RegisterEvent("CINEMATIC_START");
EventFrame:RegisterEvent("CINEMATIC_STOP");
EventFrame:RegisterEvent("CLOSE_INBOX_ITEM");
EventFrame:RegisterEvent("CLOSE_TABARD_FRAME");
EventFrame:RegisterEvent("COMBAT_LOG_EVENT");
EventFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED");
EventFrame:RegisterEvent("COMBAT_RATING_UPDATE");
EventFrame:RegisterEvent("COMBAT_TEXT_UPDATE");
EventFrame:RegisterEvent("COMMENTATOR_ENTER_WORLD");
EventFrame:RegisterEvent("COMMENTATOR_MAP_UPDATE");
EventFrame:RegisterEvent("COMMENTATOR_PLAYER_UPDATE");
EventFrame:RegisterEvent("COMPANION_LEARNED");
EventFrame:RegisterEvent("COMPANION_UNLEARNED");
EventFrame:RegisterEvent("COMPANION_UPDATE");
EventFrame:RegisterEvent("CONFIRM_BINDER");
EventFrame:RegisterEvent("CONFIRM_DISENCHANT_ROLL");
EventFrame:RegisterEvent("CONFIRM_LOOT_ROLL");
EventFrame:RegisterEvent("CONFIRM_SUMMON");
EventFrame:RegisterEvent("CONFIRM_TALENT_WIPE");
EventFrame:RegisterEvent("CONFIRM_XP_LOSS");
EventFrame:RegisterEvent("CORPSE_IN_INSTANCE");
EventFrame:RegisterEvent("CORPSE_IN_RANGE");
EventFrame:RegisterEvent("CORPSE_OUT_OF_RANGE");
EventFrame:RegisterEvent("CRITERIA_UPDATE");
EventFrame:RegisterEvent("CURRENT_SPELL_CAST_CHANGED");
EventFrame:RegisterEvent("CURRENCY_DISPLAY_UPDATE");
EventFrame:RegisterEvent("CURSOR_UPDATE");
EventFrame:RegisterEvent("CVAR_UPDATE");

EventFrame:RegisterEvent("PLAYER_LOGIN"); -- Fired when the player is about to logs in
EventFrame:RegisterEvent("PLAYER_LOGOUT"); -- Fired when the player is about to log out
EventFrame:RegisterEvent("PLAYER_ENTERING_WORLD"); -- Fired when the player enters the world

EventFrame:SetScript('OnEvent', function(self, event, arg1, ...)
    if not (JournalEvents.events[event] == nil) or (JournalEvents.events[event] == '') then
        for index, entry in pairs(JournalEvents.events[event]) do
            entry.func(self, arg1);
        end
    end
end)
