function Journal_MinimapButton_Reposition()
    Journal_MinimapButton:SetPoint("TOPLEFT", "Minimap", "TOPLEFT", 52 - (80 * cos(Journal_Settings.MinimapPos)), (80 * sin(Journal_Settings.MinimapPos)) - 52)
end

function Journal_MinimapButton_DraggingFrame_OnUpdate()
    local xpos, ypos = GetCursorPosition()
    local xmin, ymin = Minimap:GetLeft(), Minimap:GetBottom()

    xpos = xmin - xpos / UIParent:GetScale() + 70 -- get coordinates as differences from the center of the minimap
    ypos = ypos / UIParent:GetScale() - ymin - 70

    Journal_Settings.MinimapPos = math.deg(math.atan2(ypos, xpos)) -- save the degrees we are relative to the minimap center
    Journal_MinimapButton_Reposition() -- move the button
end

function Journal_MinimapButton_OnClick()
    JournalEvents.fireEvent("TOGGLE_JOURNAL");
end
